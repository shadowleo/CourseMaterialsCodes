课程资料以及代码  
================  

仓库说明  
--------  

除给班班提交作业代码之外，可以把自己的代码放在 github 上或者 gitee 上，github 连接慢的同学可以放在 gitee 上，gitee 的使用方法与 github 一样，但连接更快。  

克隆仓库  
--------  

```
git clone https://gitee.com/Mathematical_formula_recognition/CourseMaterialsCodes.git
```

代码提交  
--------  

```  
git add .
git commit -m "your content"
git push -u origin master
```  

目录结构  
--------  

- resources: 课程相关资料、电子书、论文等  
- utils: py 实用小工具  
- weekX: 第 X 周课程代码  

week17 作业  
-----------  

### 作业要求  

![](https://images.gitee.com/uploads/images/2020/0415/193515_77327d62_7401441.png)  

![](https://images.gitee.com/uploads/images/2020/0415/193530_b3b6b256_7401441.png)  

### 优秀作业  

| 学员     | 仓库链接                   | 点评                                                         |
| -------- | -------------------------- | ------------------------------------------------------------ |
| txy14326 | https://gitee.com/txy14326 | 这个代码的思路特别清晰。训练一个模型主要的步骤都有：样本数据准备，gt 的准备，模型的建立，loss 函数的定义，优化函数的选择等等。<br>在这个思路上扩充成大规模的数据集，大模型会比较容易，比较好理解。 |

![](https://images.gitee.com/uploads/images/2020/0418/123806_80214156_7401441.png)  

week18 作业  
-----------  

### 作业要求  

![](https://images.gitee.com/uploads/images/2020/0418/125454_7a298afa_7401441.jpeg)

作业建议步骤以及建议代码：  

1. 数据准备（图片读取，对应的GT生成)[数据这个没有讲，所以代码直接提供，不用填空，代码位置：week18/dataset.py]  
2. EAST 模型建立[填空代码week18/model.py]  
3. loss 函数建立[填空代码week18/loss.py]  
4. 优化函数，超参数选择[填空代码week18/train.py]  
    
> 开始作业前，建议先下载 VGG 模型预训练权重 pths.zip 和数据集 ICDAR_2015.zip。  
>
> ​    [ICDAR_2015.zip](https://pan.baidu.com/s/14f44grwKT8rAauRR1c2InA)，密码：hc1v
>
> ​    [pths.zip](https://pan.baidu.com/s/1acCEDRipkT7FOJ1wlO0Eyg)，密码：108d  
>
> 下载后解压，将 `ICDAR_2015` 和 `pths` 文件夹放入 `week18` 目录中。  

数学公式识别检测数据集：  
[cvpr2019](https://pan.baidu.com/s/100OAXTIOTPoMjbi-dwOcxA)，提取码：0sbm  
[cvpr2020](https://pan.baidu.com/share/init?surl=lo3smbFWiBSNnut9JssYaQ)，提取码：8171  

进入标注小组办法：
  - 这里是列表文本在modelarts平台上标注有账号，
  - 将modelarts绑定的邮箱发给老师，老师将你加入标注小组中。
  - 成功加入标注小组后，你会收到如下邮件：
      ![输入图片说明](https://images.gitee.com/uploads/images/2020/0429/170329_d5beb2de_7401441.png "屏幕截图.png")
 
标注办法：
  1. 点开邮件里面的链接，登陆并更改初始密码，登陆进去 标注系统
  2. 对于含有公式的图片，进行如下标注：
      ![输入图片说明](https://images.gitee.com/uploads/images/2020/0429/164600_2dc1bd5e_7401441.png "标注办法_有公式.png")
  3.  遇到不含有公式的图片，进行如下标注：
     ![输入图片说明](https://images.gitee.com/uploads/images/2020/0429/164531_2840d9c0_7401441.png "标注办法_没有公式.png")


week19 作业[计划20200427日晚更新完毕]  
-----------  

### 作业要求 
1. 完成locality-aware NMS
2. 完成EAST模型的前向计算过程

 作业建议步骤以及建议代码：
1. /week19/inference.py 88行，独立完成locality-aware NMS
2. 完成EAST模型的前向计算过程，输入一张图片，输出其相应结果。
 
 可选作业：
 1. 完成模型的评价测试工作：计算f-socre/h-mean.


Modelarts使用效果
-------------------

gpu类型：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/201206_106659bb_7401441.png "tesla_v100_32g.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/201244_1ed05270_7401441.png "modelarts_terminal.png")


训练速度特别快，数万数据，一个epoch只要45秒左右。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/201219_e3c1b5f1_7401441.png "训练过程2.png")



Modelarts使用办法
-------------------

第一次配置时间较长，建议腾出至少4个小时的时间来配置。
使用云上的独立GPU，注意一点：obs是你唯一可以保存文件的地方，其他地方的保存都是暂时的，会清除。
 pipeline:
 1. 准备好自己的gitee
 2. 建立modelarts上的notebook
 3. 打开notebook对应的terminal
 4. git clone you_program 到terminal里
 5. 利用notebook同步功能将数据同步到terminal里
 6. 数据和代码都有了，可以配置环境和开发了。


下面是详细步骤：

1. 准备好自己的gitee

2. 建立modelarts上的notebook【建议建立一个6小时的】

创建notebook
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200731_7a27883e_7401441.png "创建notebook.png")

notebook参数
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200747_31ff1e4c_7401441.png "创建notebook的参数.png")

notebook参数
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200812_a36a2930_7401441.png "创建notebook的参数1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200821_bc7f574f_7401441.png "创建notebook的参数3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200830_148d5177_7401441.png "创建notebook的参数5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200843_40baf02c_7401441.png "确认notebook的参数1.png")

从这里打开我们刚才创建的notebook环境
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200900_c8927593_7401441.png "打开notebook.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/200911_9e960111_7401441.png "打开我们刚才创建的notebook.png")



3. 打开notebook对应的terminal
         
打开terminal
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/195637_5a27d243_7401441.png "打开teminal.png")

在terminal里自己可以随意操作
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/195655_c3af4a7b_7401441.png "已经打开的terminal.png")
        
termial使用原则
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/195713_d68cf58d_7401441.png "terminal的使用办法.png")

4. git clone 你的gitee，把代码放到terminal里
     查看README文件，切换到pytorch环境，可以使用gpu

5. 利用notebook的同步功能，把数据也同步到terminal里

选中notebook里的文件，点击syn,可以将notebook里的文件同步到terminal里
![输入图片说明](https://images.gitee.com/uploads/images/2020/0428/195747_a4e06f57_7401441.png "obs同步到terminal.png")

6. 数据和代码都有了，可以配置环境和开发了。
    
  

TO DO LIST:  

1. 待添加标注工具【已完成】  
2. 待标注数学公式数据  
3. 解决loss的其中一个设计问题：负样本loss返回零
4. 收集学员modelarts绑定的邮箱，分配标注任务。

更新日志  
--------  
20200429:
1. 添加数学公式检测数据集，团队标注办法

20200428:
1. 添加modelarts使用办法


20200425:
1. 添加EAST的论文精读翻译
2. 添加EAST论文的课堂讲解
3. 添加EAST论文得作业代码inference.py

20200422:  

1. 调整目录结构  
2. 优化 README.md  

20200421:  

1. 实验添加 pdf 转图片代码
2. 整理大量 pdf 论文下载链接 


参与贡献  
--------  

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

码云特技  
--------  

1. 使用 README\_XX.md 来支持不同的语言，例如 README\_en.md, README\_zh.md  
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)  
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目  
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目  
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)  
6. 码云封面人物是一档用来展示码云会员风：采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)  
